package com.CarRentalCRUD.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.CarSize;
import com.CarRentalCRUD.repositories.OrderRepository;
import com.CarRentalCRUD.vo.CarVO;
import com.CarRentalCRUD.vo.CustomerVO;
import com.CarRentalCRUD.vo.OrderVO;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTests {

	@InjectMocks
    OrderService orderService;
     
    @Mock
    OrderRepository orderRepository;
    
    @Test
	@Order(1)
	@Rollback(value = false)
	public void saveCustomerServiceTest() {
    	CustomerVO customerVO = new CustomerVO(1, "Marcus", "Vägen 1");
    	CarVO carVO = new CarVO(1, "Volvo", CarSize.MEDIUM, 20, true);
		OrderVO orderVO = new OrderVO(1, customerVO, carVO, new Date(), true);
		
		int id = orderService.saveOrder(orderVO);
       
		assertEquals(id, 1);
	}
	
	@Test
	@Order(2)
	public void getCustomerServiceTest() {
		orderService.getAllOrders();
		
		verify(orderRepository, times(1)).findAll();
	}
	
	@Test
	@Order(3)
	@Rollback(value = false)
	public void deleteCustomerServiceTest() {
		orderService.deleteOrder(1);
		
		verify(orderRepository, times(1)).deleteById(1);
	}
}
