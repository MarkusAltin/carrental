package com.CarRentalCRUD.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.repositories.CustomerRepository;
import com.CarRentalCRUD.vo.CustomerVO;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTests {

	@InjectMocks
    CustomerService customerService;
     
    @Mock
    CustomerRepository customerRepository;
    
    @Test
	@Order(1)
	@Rollback(value = false)
	public void saveCustomerServiceTest() {
    	CustomerVO customerVO = new CustomerVO(1, "Marcus", "Vägen 1");
    	
		int id = customerService.saveCustomers(customerVO);
        
		assertEquals(id, 1);
	}
	
	@Test
	@Order(2)
	public void getCustomerServiceTest() {
		customerService.getAllCustomers();
		
		verify(customerRepository, times(1)).findAll();
	}
	
	@Test
	@Order(3)
	@Rollback(value = false)
	public void deleteCustomerServiceTest() {
		customerService.deleteCustomers(1);
		
		verify(customerRepository, times(1)).deleteById(1);
	}
}
