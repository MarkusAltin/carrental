package com.CarRentalCRUD.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.CarSize;
import com.CarRentalCRUD.repositories.CarRepository;
import com.CarRentalCRUD.vo.CarVO;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@RunWith(MockitoJUnitRunner.class)
public class CarServiceTests {

	@InjectMocks
    CarService carService;
     
    @Mock
    CarRepository carRepository;
    
    @Test
	@Order(1)
	@Rollback(value = false)
	public void saveCarServiceTest() {
    	CarVO carVO = new CarVO(1, "Volvo", CarSize.MEDIUM, 20, true);
    	
		int id = carService.saveCar(carVO);
        
		assertEquals(id, 1);
	}
	
	@Test
	@Order(2)
	public void getCarsServiceTest() {
		carService.getAllCars();
		
		verify(carRepository, times(1)).findAll();
	}
}
