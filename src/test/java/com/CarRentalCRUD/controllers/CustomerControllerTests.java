package com.CarRentalCRUD.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.services.CustomerServiceInterface;
import com.CarRentalCRUD.vo.CustomerVO;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTests {

	@InjectMocks
    CustomerController customerController;
     
    @Mock
    CustomerServiceInterface customerService;
    
    @Test
	@Order(1)
	@Rollback(value = false)
	public void saveCustomerControllerTest() {
    	CustomerVO customer = new CustomerVO(1, "Marcus", "Vägen 1");
//		Customer customer = new Customer();
//		customer.setName("Marcus");
//		customer.setAdress("Vägen 1");
		customerController.saveCustomer(customer);
        
        verify(customerService, times(1)).saveCustomers(customer);
	}
	
	@Test
	@Order(2)
	public void getCustomerControllerTest() {
		customerController.getAllCustomers();
		
		verify(customerService, times(1)).getAllCustomers();
	}
	
	@Test
	@Order(3)
	@Rollback(value = false)
	public void deleteCustomerControllerTest() {
		customerController.deleteCustomers(1);
		
		verify(customerService, times(1)).deleteCustomers(1);
	}
}
