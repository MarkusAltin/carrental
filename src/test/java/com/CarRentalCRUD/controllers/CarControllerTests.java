package com.CarRentalCRUD.controllers;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.CarSize;
import com.CarRentalCRUD.services.CarServiceInterface;
import com.CarRentalCRUD.vo.CarVO;


@ExtendWith(MockitoExtension.class)
public class CarControllerTests {
	
	@InjectMocks
    CarController carController;
     
    @Mock
    CarServiceInterface carService;
    
    @Test
	@Order(1)
	@Rollback(value = false)
	public void saveCarControllerTest() {
		CarVO car = new CarVO(1, "Volvo", CarSize.MEDIUM, 20, true);
		carController.saveCar(car);
        
        verify(carService, times(1)).saveCar(car);
	}
	
	@Test
	@Order(2)
	public void getCarsControllerTest() {
		carController.getAllCars();
		
		verify(carService, times(1)).getAllCars();
	}
	
	@Test
	@Order(3)
	@Rollback(value = false)
	public void deleteCarControllerTest() {
		carController.deleteCar(1);
		
		verify(carService, times(1)).deleteCar(1);
	}
}
