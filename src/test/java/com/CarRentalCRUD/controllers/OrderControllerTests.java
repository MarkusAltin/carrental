package com.CarRentalCRUD.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.CarSize;
import com.CarRentalCRUD.services.OrderServiceInterface;
import com.CarRentalCRUD.vo.CarVO;
import com.CarRentalCRUD.vo.CustomerVO;
import com.CarRentalCRUD.vo.OrderVO;

@ExtendWith(MockitoExtension.class)
public class OrderControllerTests {

	@InjectMocks
    OrderController orderController;
     
    @Mock
    OrderServiceInterface orderService;
    
    @Test
	@Order(1)
	@Rollback(value = false)
	public void saveOrderControllerTest() {
    	CustomerVO customer = new CustomerVO(1, "Marcus", "Vägen 1");
    	CarVO car = new CarVO(1, "Volvo", CarSize.MEDIUM, 20, true);
    	OrderVO order = new OrderVO(1, customer, car, new Date(), true);

		orderController.saveOrder(order);
        
        verify(orderService, times(1)).saveOrder(order);
	}
	
	@Test
	@Order(2)
	public void getOrderControllerTest() {
		orderController.getAllOrders();
		
		verify(orderService, times(1)).getAllOrders();
	}
	
	@Test
	@Order(3)
	@Rollback(value = false)
	public void deleteOrderControllerTest() {
		orderController.deleteOrder(1);
		
		verify(orderService, times(1)).deleteOrder(1);
	}
}
