package com.CarRentalCRUD.repositories;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.Customer;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CustomerRepositoryTests {

	@Autowired CustomerRepository customerRepository;

	@Test
	@Order(1)
	@Rollback(value = false)
	public void saveCustomerTest() {
		Customer customer = new Customer();
		customer.setName("Marcus");
		customer.setAdress("Coolgatan 42");
		
		customerRepository.save(customer);
		Assertions.assertThat(customer.getId()).isGreaterThan(0);
	}
	
	@Test
	@Order(2)
	public void getCustomerTest() {
		Customer customer = customerRepository.findById(1).get();
		Assertions.assertThat(customer.getId()).isEqualTo(1);
	}
	
	@Test
	@Order(3)
	public void getCustomersTest() {
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		Assertions.assertThat(customers.size()).isGreaterThan(0);
	}
	
	@Test
	@Order(4)
	@Rollback(value = false)
	public void updateCustomerTest() {
		Customer customer = customerRepository.findById(1).get();
		customer.setName("Klas");
		Customer customerUpdated = customerRepository.save(customer);
		
		Assertions.assertThat(customerUpdated.getName()).isEqualTo("Klas");
	}
}
