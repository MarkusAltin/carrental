package com.CarRentalCRUD.repositories;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.Car;
import com.CarRentalCRUD.models.CarSize;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CarRepositoryTests {
	
	@Autowired CarRepository carRepository;

	@Test
	@Order(1)
	@Rollback(value = false)
	public void saveCarTest() {
		Car car = new Car();
		car.setBrand("Volvo");
		car.setSize(CarSize.MEDIUM);
		car.setPrice(20);
		car.setActive(true);
		
		carRepository.save(car);
		Assertions.assertThat(car.getId()).isGreaterThan(0);
	}
	
	@Test
	@Order(2)
	public void getCarTest() {
		Car car = carRepository.findById(1).get();
		Assertions.assertThat(car.getId()).isEqualTo(1);
	}
	
	@Test
	@Order(3)
	public void getCarsTest() {
		List<Car> cars = (List<Car>) carRepository.findAll();
		Assertions.assertThat(cars.size()).isGreaterThan(0);
	}
	
	@Test
	@Order(4)
	@Rollback(value = false)
	public void updateCarTest() {
		Car car = carRepository.findById(1).get();
		car.setBrand("Audi");
		Car carUpdated = carRepository.save(car);
		
		Assertions.assertThat(carUpdated.getBrand()).isEqualTo("Audi");
	}
}
