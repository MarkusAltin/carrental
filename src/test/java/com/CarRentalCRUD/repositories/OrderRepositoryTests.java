package com.CarRentalCRUD.repositories;


import java.util.Date;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.CarRentalCRUD.models.Car;
import com.CarRentalCRUD.models.CarSize;
import com.CarRentalCRUD.models.Customer;
import com.CarRentalCRUD.models.CarOrder;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderRepositoryTests {

	@Autowired OrderRepository orderRepository;
	@Autowired CustomerRepository customerRepository;
	@Autowired CarRepository carRepository;

	@Test
	@Order(1)
	@Rollback(value = false)
	public void saveOrderTest() {
		CarOrder order = new CarOrder(); 
		
		Customer customer = new Customer();
		customer.setName("Marcus");
		customer.setAdress("Coolgatan 42");
		customerRepository.save(customer);
		
		Car car = new Car();
		car.setBrand("Volvo");
		car.setSize(CarSize.MEDIUM);
		car.setPrice(20);
		car.setActive(true);
		carRepository.save(car);
		
		order.setCustomer(customer);
		order.setCar(car);
		order.setDate(new Date());
		order.setActive(true);
		
		orderRepository.save(order);
		Assertions.assertThat(order.getId()).isGreaterThan(0);
	}
	
	@Test
	@Order(2)
	public void getOrderTest() {
		CarOrder order = orderRepository.findById(1).get();
		Assertions.assertThat(order.getId()).isEqualTo(1);
	}
	
	@Test
	@Order(3)
	public void getOrdersTest() {
		List<CarOrder> orders = (List<CarOrder>) orderRepository.findAll();
		Assertions.assertThat(orders.size()).isGreaterThan(0);
	}
	
	@Test
	@Order(4)
	@Rollback(value = false)
	public void updateOrderTest() {
		CarOrder order = orderRepository.findById(1).get();
		Date newDate = new Date();
		order.setDate(newDate);
		CarOrder orderUpdated = orderRepository.save(order);
		
		Assertions.assertThat(orderUpdated.getDate()).isEqualTo(newDate);
	}
	
	@Test
	@Order(5)
	@Rollback(value = false)
	public void deleteOrderTest() {
		CarOrder order = orderRepository.findById(1).get();
		orderRepository.delete(order);
		
		List<CarOrder> orders = (List<CarOrder>) orderRepository.findAll();
		Assertions.assertThat(orders.get(0).getActive()).isEqualTo(false);
	}
}
