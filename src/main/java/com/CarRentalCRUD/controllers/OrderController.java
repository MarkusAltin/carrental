package com.CarRentalCRUD.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.CarRentalCRUD.services.OrderServiceInterface;
import com.CarRentalCRUD.vo.OrderVO;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class OrderController {

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired OrderServiceInterface orderService;
	
	@GetMapping("/myorders")
	public List<OrderVO> getAllOrders () {
		return orderService.getAllOrders();
	}
	
	@GetMapping("/order/{id}")
	public OrderVO getOrder(@PathVariable("id") int id) {
		return orderService.getOrder(id);
	}
	
	@DeleteMapping("/order/{id}")
	public void deleteOrder(@PathVariable("id") int id) {
		logger.info("deleteOrder was called");
		orderService.deleteOrder(id);
	}
	
	@PostMapping("/ordercar")
	public int saveOrder(@RequestBody OrderVO order) {
		orderService.saveOrder(order);
		logger.info("saveOrder was called");
		return order.getId();
	}
	
	@PutMapping("/updateorder")
	public int updateOrder(@RequestBody OrderVO order) {
		orderService.saveOrder(order);
		logger.info("updateOrder was called");
		return order.getId();
	}
}
