package com.CarRentalCRUD.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.CarRentalCRUD.services.CustomerServiceInterface;
import com.CarRentalCRUD.vo.CustomerVO;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired CustomerServiceInterface customerService;
	
	@GetMapping("/customers")
	public List<CustomerVO> getAllCustomers () {
		return customerService.getAllCustomers();
	}
	
	@GetMapping("/customer/{id}")
	public CustomerVO getCustomers(@PathVariable("id") int id) {
		return customerService.getCustomers(id);
	}
	
	@DeleteMapping("/customer/{id}")
	public void deleteCustomers(@PathVariable("id") int id) {
		logger.info("deleteCustomer was called");
		customerService.deleteCustomers(id);
	}
	
	@PostMapping("/customer")
	public int saveCustomer(@RequestBody CustomerVO customer) {
		logger.info("saveCustomer was called");
		customerService.saveCustomers(customer);
		return customer.getId();
	}
	
	@PutMapping("/updatecustomer")
	public int updateCustomer(@RequestBody CustomerVO customer) {
		logger.info("updateCustomer was called");
		customerService.saveCustomers(customer);
		return customer.getId();
	}
}
