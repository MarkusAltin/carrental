package com.CarRentalCRUD.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.CarRentalCRUD.services.CarServiceInterface;
import com.CarRentalCRUD.vo.CarVO;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class CarController {

	private static final Logger logger = LoggerFactory.getLogger(CarController.class);
	
	@Autowired CarServiceInterface carService;
	
	//@CrossOrigin("*")
	@GetMapping("/cars")
	public List<CarVO> getAllCars () {
		return carService.getAllCars();
	}
	
	@GetMapping("/car/{id}")
	public CarVO getCar(@PathVariable("id") int id) {
		return carService.getCar(id);
	}
	
	@DeleteMapping("/deletecar/{id}")
	public void deleteCar(@PathVariable("id") int id) {
		logger.info("DeleteCar was called");
		carService.deleteCar(id);
	}
	
	@PostMapping("/addcar")
	public int saveCar(@RequestBody CarVO car) {
		logger.info("saveCar was called");
		carService.saveCar(car);
		return car.getId();
	}
	
	@PutMapping("/updatecar")
	public int updateCar(@RequestBody CarVO car) {
		logger.info("updateCar was called");
		carService.updateCar(car);
		return car.getId();
	}
}
