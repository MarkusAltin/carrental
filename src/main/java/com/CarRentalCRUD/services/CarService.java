package com.CarRentalCRUD.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CarRentalCRUD.models.Car;
import com.CarRentalCRUD.repositories.CarRepository;
import com.CarRentalCRUD.repositories.OrderRepository;
import com.CarRentalCRUD.vo.CarVO;

@Service
public class CarService implements CarServiceInterface {

	@Autowired CarRepository carRepository;
	@Autowired OrderRepository orderRepository;
	
	@Override
	public List<CarVO> getAllCars() {
		List<Car> cars = new ArrayList<Car>();
		List<CarVO> carsVO = new ArrayList<CarVO>(); 
		carRepository.findAll().forEach(result -> cars.add(result));
		for (Car car : cars) {
			if (car.getActive() == true) {
				carsVO.add(carToVO(car));
			}
		}
		return carsVO;
	}

	@Override
	public CarVO getCar(int id) {
		return carToVO(carRepository.findById(id).get());
	}

	@Override
	public void deleteCar(int id) {
		Car car = carRepository.findById(id).get();
		car.setActive(false);
		carRepository.save(car);
	}

	@Override
	public int saveCar(CarVO carVO) {
		carVO.setActive(true);
		carRepository.save(carVOToCar(carVO));
		return carVO.getId();
	}
	
	@Override
	public int updateCar(CarVO carVO) {
		Car car = carVOToCar(carVO);
		car.setId(carVO.getId());
		carRepository.save(car);
		return carVO.getId();
	}
	
	private CarVO carToVO (Car car) {
		CarVO carVO = new CarVO(car.getId(), car.getBrand(), car.getSize(), car.getPrice(), car.getActive());
		return carVO;
	}
	
	private Car carVOToCar (CarVO carVO) {
		Car car = new Car();
		car.setBrand(carVO.getBrand());
		car.setSize(carVO.getSize());
		car.setPrice(carVO.getPrice());
		car.setActive(carVO.getActive());
		return car;
	}

}
