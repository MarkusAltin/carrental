package com.CarRentalCRUD.services;

import java.util.List;

import com.CarRentalCRUD.vo.CarVO;

public interface CarServiceInterface {

	List<CarVO> getAllCars();

	CarVO getCar(int id);

	void deleteCar(int id);

	int saveCar(CarVO car);

	int updateCar(CarVO car);

}