package com.CarRentalCRUD.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CarRentalCRUD.models.Customer;
import com.CarRentalCRUD.repositories.CustomerRepository;
import com.CarRentalCRUD.vo.CustomerVO;

@Service
public class CustomerService implements CustomerServiceInterface {

	@Autowired CustomerRepository customerRepository;
	
	@Override
	public List<CustomerVO> getAllCustomers() {
		List<Customer> customers = new ArrayList<Customer>();  
		List<CustomerVO> customersVO = new ArrayList<CustomerVO>(); 
		customerRepository.findAll().forEach(result -> customers.add(result));
		for (Customer customer : customers) {
			customersVO.add(customerToVO(customer));
		}
		return customersVO;
	}

	@Override
	public CustomerVO getCustomers(int id) {
		return customerToVO(customerRepository.findById(id).get());
	}

	@Override
	public void deleteCustomers(int id) {
		customerRepository.deleteById(id);
	}

	@Override
	public int saveCustomers(CustomerVO customer) {
		customerRepository.save(vOToCustomer(customer));
		return customer.getId();
	}
	
	@Override
	public int updateCustomer(CustomerVO customer) {
		customerRepository.save(vOToCustomer(customer));
		return customer.getId();
	}
	
	private CustomerVO customerToVO (Customer customer) {
		CustomerVO customerVO = new CustomerVO(customer.getId(), customer.getName(), customer.getAdress());
		return customerVO;
	}
	
	private Customer vOToCustomer (CustomerVO customerVO) {
		Customer customer = new Customer();
		customer.setName(customerVO.getName());
		customer.setAdress(customerVO.getAdress());
		return customer;
	}

}
