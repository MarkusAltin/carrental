package com.CarRentalCRUD.services;

import java.util.List;

import com.CarRentalCRUD.vo.OrderVO;

public interface OrderServiceInterface {

	List<OrderVO> getAllOrders();

	OrderVO getOrder(int id);

	void deleteOrder(int id);

	int saveOrder(OrderVO order);

	int updateOrder(OrderVO orderVO);

}