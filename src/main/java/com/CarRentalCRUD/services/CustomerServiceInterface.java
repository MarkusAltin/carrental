package com.CarRentalCRUD.services;

import java.util.List;

import com.CarRentalCRUD.vo.CustomerVO;

public interface CustomerServiceInterface {

	List<CustomerVO> getAllCustomers();

	CustomerVO getCustomers(int id);

	void deleteCustomers(int id);

	int saveCustomers(CustomerVO customer);

	int updateCustomer(CustomerVO customer);

}