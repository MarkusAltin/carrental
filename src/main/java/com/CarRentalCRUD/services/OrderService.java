package com.CarRentalCRUD.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CarRentalCRUD.models.Car;
import com.CarRentalCRUD.models.CarOrder;
import com.CarRentalCRUD.models.Customer;
import com.CarRentalCRUD.repositories.OrderRepository;
import com.CarRentalCRUD.vo.CarVO;
import com.CarRentalCRUD.vo.CustomerVO;
import com.CarRentalCRUD.vo.OrderVO;

@Service
public class OrderService implements OrderServiceInterface {

	@Autowired OrderRepository orderRepository;
	
	@Override
	public List<OrderVO> getAllOrders() {
		List<CarOrder> orders = new ArrayList<CarOrder>();  
		List<OrderVO> ordersVO = new ArrayList<OrderVO>();
		orderRepository.findAll().forEach(result -> orders.add(result));
		for (CarOrder order : orders) {
			ordersVO.add(orderToVO(order));
		}
		return ordersVO;
	}

	@Override
	public OrderVO getOrder(int id) {
		return orderToVO(orderRepository.findById(id).get());
	}

	@Override
	public void deleteOrder(int id) {
		orderRepository.deleteById(id);
	}

	@Override
	public int saveOrder(OrderVO order) {
		CarOrder carOrder;
		carOrder = orderVOToOrder(order);
		orderRepository.save(carOrder);
		return order.getId();
	}
	
	@Override
	public int updateOrder(OrderVO orderVO) {
		orderRepository.save(orderVOToOrder(orderVO));
		return orderVO.getId();
	}

	private OrderVO orderToVO (CarOrder order) {
		CarVO carVO = null;
		CustomerVO customerVO = null;
		if (order.getCustomer() != null) {
			Customer customer = order.getCustomer();
			customerVO = new CustomerVO(customer.getId(), customer.getName(), customer.getAdress());
		} else {
		}
		if (order.getCar() != null) {
			Car car = order.getCar();
			carVO = new CarVO(car.getId(), car.getBrand(), car.getSize(), car.getPrice(), car.getActive());
		}else {
		}
		OrderVO orderVo = new OrderVO(order.getId(), customerVO, carVO, order.getDate(), order.getActive());
		return orderVo;
	}
	
	private CarOrder orderVOToOrder (OrderVO orderVO) {
		CustomerVO customerVO = orderVO.getCustomer();
		Customer customer = new Customer();
		customer.setId(customerVO.getId());
		customer.setName(customerVO.getName());
		customer.setAdress(customerVO.getAdress());
		CarVO carVO = orderVO.getCar();
		Car car = new Car();
		car.setId(carVO.getId());
		car.setBrand(carVO.getBrand());
		car.setSize(carVO.getSize());
		car.setPrice(carVO.getPrice());
		car.setActive(carVO.getActive());
		CarOrder order = new CarOrder();
		order.setId(orderVO.getId());
		order.setCustomer(customer);
		order.setCar(car);
		order.setDate(orderVO.getDate());
		order.setActive(orderVO.getActive());
		return order;
	}
}
