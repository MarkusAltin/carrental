package com.CarRentalCRUD.models;

public enum CarSize {

	SMALL,
	MEDIUM,
	LARGE
}
