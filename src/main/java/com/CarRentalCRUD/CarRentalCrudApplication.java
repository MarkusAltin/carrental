package com.CarRentalCRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarRentalCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarRentalCrudApplication.class, args);
	}

}
