package com.CarRentalCRUD.security;

import java.util.Arrays;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class BasicConfig extends WebSecurityConfigurerAdapter {

	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder =
                PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth
            .inMemoryAuthentication()
            .withUser("user")
            .password(encoder.encode("password"))
            .roles("USER")
            .and()
            .withUser("admin")
            .password(encoder.encode("admin"))
            .roles("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/cars").hasRole("USER")
            .antMatchers("/ordercar").hasRole("USER")
            .antMatchers("/updateorder").hasRole("USER")
            .antMatchers("/myorders").hasRole("USER")
            .antMatchers("/customers").hasRole("ADMIN")
            .antMatchers("/addcar").hasRole("ADMIN")
            .antMatchers("/deletecar/{\\d+}").hasRole("ADMIN")
            .antMatchers("/updatecar").hasRole("ADMIN")
            .antMatchers("/car/{\\d+}").hasRole("ADMIN")
            .antMatchers("/order/{\\d+}").hasRole("ADMIN")
            .antMatchers("/customer/{\\d+}").hasRole("ADMIN")
            .antMatchers("/customer").hasRole("ADMIN")
            .antMatchers("/updatecustomer").hasRole("ADMIN")
            .anyRequest()
            .authenticated()
            .and()
            .httpBasic();
        
        http.csrf().disable();
        http.cors(c ->{
            CorsConfigurationSource cs = request -> {
                CorsConfiguration cc = new CorsConfiguration();
                cc.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
                cc.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"));
                cc.setAllowedHeaders(Arrays.asList("ORIGIN", "CONTENT-TYPE", "AUTHORIZATION", "BODY"));
        return cc;
            };
            c.configurationSource(cs);
        });
    }
}
