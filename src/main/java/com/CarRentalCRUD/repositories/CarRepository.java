package com.CarRentalCRUD.repositories;

import org.springframework.data.repository.CrudRepository;

import com.CarRentalCRUD.models.Car;

public interface CarRepository extends CrudRepository <Car, Integer>{

}
