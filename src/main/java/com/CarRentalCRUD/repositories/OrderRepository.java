package com.CarRentalCRUD.repositories;

import org.springframework.data.repository.CrudRepository;

import com.CarRentalCRUD.models.CarOrder;

public interface OrderRepository extends CrudRepository <CarOrder, Integer> {

}
