package com.CarRentalCRUD.repositories;

import org.springframework.data.repository.CrudRepository;

import com.CarRentalCRUD.models.Customer;

public interface CustomerRepository extends CrudRepository <Customer, Integer> {

}
