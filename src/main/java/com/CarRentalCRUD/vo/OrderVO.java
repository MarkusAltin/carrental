package com.CarRentalCRUD.vo;

import java.util.Date;


public class OrderVO {

	private int id;
	private CustomerVO customer;
	private CarVO car;
	private Date date;
	private boolean active;

	public OrderVO(int id, CustomerVO customer, CarVO car, Date date, boolean active) {
		super();
		this.id = id;
		this.customer = customer;
		this.car = car;
		this.date = date;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CustomerVO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerVO customer) {
		this.customer = customer;
	}

	public CarVO getCar() {
		return car;
	}

	public void setCar(CarVO car) {
		this.car = car;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
