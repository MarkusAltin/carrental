package com.CarRentalCRUD.vo;

import com.CarRentalCRUD.models.CarSize;

public class CarVO {

	private int id;
	private String brand;
	private CarSize size;
	private double price;
	private boolean active;
	
	public CarVO(int id, String brand, CarSize size, double price, boolean active) {
		super();
		this.id = id;
		this.brand = brand;
		this.size = size;
		this.price = price;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public CarSize getSize() {
		return size;
	}

	public void setSize(CarSize size) {
		this.size = size;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
