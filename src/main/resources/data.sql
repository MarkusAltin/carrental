INSERT INTO Cars (brand, size, price, active) VALUES ('Volvo', 'SMALL', 25, true);
INSERT INTO Cars (brand, size, price, active) VALUES ('Audi', 'LARGE', 40, true);
INSERT INTO Cars (brand, size, price, active) VALUES ('SAAB', 'MEDIUM', 30, true);
INSERT INTO Cars (brand, size, price, active) VALUES ('Renault', 'SMALL', 25, true);

INSERT INTO Customers (name, adress) VALUES ('Jakop', 'Vägen 42');
INSERT INTO Customers (name, adress) VALUES ('Jenny', 'Vägen 42');
INSERT INTO Customers (name, adress) VALUES ('Klas', 'Vägen 42');
INSERT INTO Customers (name, adress) VALUES ('username', 'Vägen 42');

INSERT INTO Orders (customer, car, date, active) VALUES (1, 2, '2022-03-28', true);
INSERT INTO Orders (customer, car, date, active) VALUES (1, 1, '2021-02-28', false);
INSERT INTO Orders (customer, car, date, active) VALUES (1, 3, '2020-04-15', false);
INSERT INTO Orders (customer, car, date, active) VALUES (1, 4, '42-12-24', false);